#include <iostream>
#include <time.h>
#include <math.h>

void a_b_c()
{
    srand(time(NULL));
    int N, Sum = 0;
    std::cout << "Size: ";
    scanf("%d", &N);
    int m[N];
    for(int i = 0; i < N; i++)
    {
        m[i] = rand();
        std::cout << m[i] << "\n";

        if(m[i]%2 == 0)
        {
            Sum += m[i];
        }
    }

    int minimum = m[0], maximum = m[0];

    for(int i = 1; i < N; i++)
    {
        if(m[i] < minimum)
            minimum = m[i];
        else if(m[i] > maximum)
            maximum = m[i];
    }
    std::cout << "Minimum:" << minimum << "\n" << "Maximum:" << maximum;
    std::cout << "\n" << "Suma = " << Sum;
}

void d()
{
    int M, y0, y1 = 0, y2 = 0;
    std::cout << "\nPoly power: ";
    scanf("%d", &M);
    int p[M];
    for(int k = 0; k <= M; k++)
    {
        std::cout << "Koef " << k+1 << ": ";
        scanf("%d", &p[k]);
        std::cout << "\n";
    }

    for(int j = 0; j <= M; j++)
    {
        y2 += p[j]*pow(2, M - j);
        y1 += p[j];
    }
    y0 = p[M];

    std::cout << "x = 0: " << y0 << "\n";
    std::cout << "x = 1: " << y1 << "\n";
    std::cout << "x = 2: " << y2 << "\n";
}
int main()
{
    int Num;
    std::cout << "For a-c write 1;"<<"\n"<<" For d write 2" ;
    scanf("%d", &Num);

    switch(Num)
    {
        case 1:
        {
            a_b_c();
            break;
        }

        case 2:
        {
            d();
            break;
        }

        default:
            break;
    }
}